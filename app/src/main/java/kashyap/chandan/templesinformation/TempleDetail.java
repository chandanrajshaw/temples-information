package kashyap.chandan.templesinformation;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.templesinformation.ResponseClasses.TempleDetailResponse;
import kashyap.chandan.templesinformation.ResponseClasses.TempleListResponse;
import kashyap.chandan.templesinformation.Retrofit.APIInterface;
import kashyap.chandan.templesinformation.Retrofit.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TempleDetail extends AppCompatActivity {
RecyclerView gallery;
Toolbar toolbar;
ImageView goback;
GalleryImageAdapter galleryImageAdapter;
Intent intent;
Dialog progressDialog;
TextView templeName,templeAddress,locationAddress,email,phone,website,about ;
    ImageView templeimage,iv_googleLocation,iv_phoneIcon;
    String id;
    Thread templeDetailThread;
    List<String> galleryImage=new ArrayList<>();
    TempleDetailResponse templeDetailResponse;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temple_detail);
        init();
//        setSupportActionBar(toolbar);
       id=intent.getStringExtra("id");
System.out.println(id);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getTemple();
        iv_phoneIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String mob=templeDetailResponse.getTemple_detail().getMobile_no();
                if (mob!=null|| !mob.isEmpty())
                {
                    Uri u = Uri.fromParts("tel",mob,null);
                    Intent i = new Intent(Intent.ACTION_DIAL, u);
                    startActivity(i);
                }
            }
        });
    }

    private  void getTemple()
    {gallery.setAdapter(null);
    galleryImage.clear();
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<TempleDetailResponse> call=apiInterface.getTempleDetails(id);
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<TempleDetailResponse>() {
                    @Override
                    public void onResponse(Call<TempleDetailResponse> call, Response<TempleDetailResponse> response) {

                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                             templeDetailResponse=response.body();
                            templeName.setText(response.body().getTemple_detail().getTemple_name());
                            templeAddress.setText(response.body().getTemple_detail().getState_name()+","+response.body().getTemple_detail().getCity_name()+","+response.body().getTemple_detail().getArea_name());
                            locationAddress.setText(response.body().getTemple_detail().getAddress());
                            email.setText(response.body().getTemple_detail().getEmail());
                            phone.setText(response.body().getTemple_detail().getMobile_no());
                            website.setText(response.body().getTemple_detail().getWebsite());
                            about.setText(response.body().getTemple_detail().getTemple_description());
                            Picasso.get().load("http://www.igranddeveloper.xyz/templeinfo/"+response.body().getTemple_detail().getTimage())
                                    .placeholder(R.drawable.loading).error(R.drawable.ic_no_preview).into(templeimage);
                            String pics = response.body().getTemple_detail().getImage();
                            String[] arrOfTempleImage = pics.split(",");
                            if (arrOfTempleImage!=null||arrOfTempleImage.length!=0)
                            {
                                for (String a : arrOfTempleImage)
                               galleryImage.add(a);
                                galleryImageAdapter=new GalleryImageAdapter(TempleDetail.this,galleryImage, new CustomItemClickListener() {
                                    @Override
                                    public void onItemClick(View v, String id, String value) {
                                        System.out.println(value);
                                        final Dialog dialog=new Dialog(TempleDetail.this);
                                        dialog.setContentView(R.layout.image_dialog);
                                        dialog.setCancelable(false);
                                            dialog.show();
                                        ImageView imageView=dialog.findViewById(R.id.imageView);
                                        ImageView close=dialog.findViewById(R.id.close);
                                        Picasso.get().load("http://www.igranddeveloper.xyz/templeinfo/"+value).placeholder(R.drawable.loading).error(R.drawable.ic_no_preview).into(imageView);
                                        DisplayMetrics metrics=getResources().getDisplayMetrics();
                                        int width=metrics.widthPixels;
                                        dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                        Window window = dialog.getWindow();
                                        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
                                        close.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                dialog.dismiss();
                                            }
                                        });
                                    }
                                }) ;
                                gallery.setAdapter(galleryImageAdapter);
                            }
                            else
                            {
                                gallery.setVisibility(View.GONE);
                            }

                        }
                        else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(TempleDetail.this, "No Data Found", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<TempleDetailResponse> call, Throwable t) {
                        progressDialog.dismiss();

                    }
                });
            }
        };
        templeDetailThread =new Thread(runnable);
        templeDetailThread.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        templeDetailThread.interrupt();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        templeDetailThread.interrupt();
    }

    private void init() {

        templeName=findViewById(R.id.templeName) ;
        templeAddress=findViewById(R.id.templeAddress);
        locationAddress=findViewById(R.id.locationAddress);
        email=findViewById(R.id.email);
        phone=findViewById(R.id.phone);
        website=findViewById(R.id.website);
        about=findViewById(R.id.about);
        intent=getIntent();
        progressDialog=new Dialog(TempleDetail.this);
        toolbar=findViewById(R.id.generalToolbar);
        gallery=findViewById(R.id.gallery);
        goback=findViewById(R.id.goback);
        templeimage =findViewById(R.id.image);
        iv_googleLocation=findViewById(R.id.iv_googleLocation);
        iv_phoneIcon=findViewById(R.id.phoneIcon);
        gallery.setLayoutManager(new LinearLayoutManager(TempleDetail.this,LinearLayoutManager.HORIZONTAL,false));

    }
}


//   holder.imgviewmap.setOnClickListener(new View.OnClickListener() {
//@Override
//public void onClick(View view) {
//        String geoUri = "http://maps.google.com/maps?q=loc:" + Double.valueOf(dataBeanList.get(position).getLatitude()) + "," + Double.valueOf(dataBeanList.get(position).getLongitude()) + " Shop";
//        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
//        context.startActivity(intent);
//        }
//        });
