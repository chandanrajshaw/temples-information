package kashyap.chandan.templesinformation;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import kashyap.chandan.templesinformation.ResponseClasses.TempleListResponse;
import kashyap.chandan.templesinformation.Retrofit.APIInterface;
import kashyap.chandan.templesinformation.Retrofit.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TempleList extends AppCompatActivity {
RecyclerView templeList;
TextView toolHeader;
ImageView goback;
TempleListAdapter templeListAdapter;
    Thread templeListThread;
    Dialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temple_list);
        init();
        toolHeader.setText("Temple List");

        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTemples();
    }

    private void init() {
        goback=findViewById(R.id.goback);
        templeList=findViewById(R.id.templeList);
        toolHeader=findViewById(R.id.toolgenheader);
        progressDialog=new Dialog(TempleList.this);
        templeList.setLayoutManager(new LinearLayoutManager(TempleList.this,LinearLayoutManager.VERTICAL,false));

    }
    private  void setTemples()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<TempleListResponse> call=apiInterface.templeList();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<TempleListResponse>() {
                    @Override
                    public void onResponse(Call<TempleListResponse> call, Response<TempleListResponse> response) {
                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            templeList.setAdapter(new TempleListAdapter(TempleList.this,response.body().getData(), new CustomItemClickListener() {
                                @Override
                                public void onItemClick(View v, String id, String value) {
                                    Intent intent=new Intent(TempleList.this,TempleDetail.class);
                                    intent.putExtra("id",id);
                                    startActivity(intent);
                                }
                            }));


                        }
                        else
                        {
                            progressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<TempleListResponse> call, Throwable t) {
                        progressDialog.dismiss();

                    }
                });
            }
        };
        templeListThread =new Thread(runnable);
        templeListThread.start();
    }
}