package kashyap.chandan.templesinformation;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class GalleryImageAdapter extends RecyclerView.Adapter<GalleryImageAdapter.MyViewHolder> {
    Context context;
    List<String> gallery;
    CustomItemClickListener listener;
    public GalleryImageAdapter(Context context, List<String> galleryImage, CustomItemClickListener listener) {
        this.context=context;
this.gallery=galleryImage;
this.listener=listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.gallery_images,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load("http://www.igranddeveloper.xyz/templeinfo/"+gallery.get(position)).placeholder(R.drawable.loading)
                .error(R.drawable.ic_no_preview).into(holder.galleryImage);
    }

    @Override
    public int getItemCount() {
        return gallery.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView galleryImage;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            galleryImage=itemView.findViewById(R.id.galleryImage);
            View.OnClickListener clickListener=new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(view,"",gallery.get(getAdapterPosition()));
                }
            };
            galleryImage.setOnClickListener(clickListener);
            itemView.setOnClickListener(clickListener);
        }
    }
}
