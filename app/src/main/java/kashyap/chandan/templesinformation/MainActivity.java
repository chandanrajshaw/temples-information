package kashyap.chandan.templesinformation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import kashyap.chandan.templesinformation.ResponseClasses.TempleListResponse;
import kashyap.chandan.templesinformation.Retrofit.APIInterface;
import kashyap.chandan.templesinformation.Retrofit.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
Toolbar toolbar;
DrawerLayout drawerLayout;
RecyclerView templeList,imageSlider;
TextView tvTitle;
ImageView right_scroll,left_scroll;
RelativeLayout nav_temple_layout,nav_home_layout;
LinearLayoutManager sliderLayoutManager;
BottomNavigationView bottomNavigationView;
Dialog progressDialog;
Thread templeListThread;
    List<TempleListResponse.DataBean> dataBean=new ArrayList<>();

    ArrayList templeImage = new ArrayList<>(Arrays.asList(R.mipmap.temple, R.drawable.temple1,R.drawable.temple2,R.drawable.temple3,R.drawable.temple4,R.mipmap.temple, R.drawable.temple1,R.drawable.temple2,R.drawable.temple3,R.drawable.temple4));
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        tvTitle.setText("Hyderabad,Ameerpet");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.list_view);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        ActionBarDrawerToggle toggle=new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.navigation_drawer_open,R.string.navigation_drawer_close){
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }
        };
        drawerLayout.addDrawerListener(toggle);
        toggle.setHomeAsUpIndicator(R.drawable.ic_baseline_list);
        toggle.syncState();
        sliderLayoutManager=new LinearLayoutManager(MainActivity.this,LinearLayoutManager.HORIZONTAL,false);
        imageSlider.setLayoutManager(sliderLayoutManager);
        imageSlider.setAdapter(new SliderImageAdapter(MainActivity.this,templeImage));
        templeList.setLayoutManager(new LinearLayoutManager(MainActivity.this,LinearLayoutManager.VERTICAL,false));
//        setTemples();
        templeList.setAdapter(new TempleListAdapter(MainActivity.this,dataBean, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, String id, String value) {
                Intent intent=new Intent(MainActivity.this,TempleDetail.class);
                intent.putExtra("id",id);
                startActivity(intent);
            }
        }));
        nav_temple_layout.setOnClickListener(this);
         nav_home_layout.setOnClickListener(this);
        left_scroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sliderLayoutManager.findFirstVisibleItemPosition() > 0) {
                    imageSlider.smoothScrollToPosition(sliderLayoutManager.findFirstVisibleItemPosition() - 1);
                } else {
                    imageSlider.smoothScrollToPosition(0);
                }

            }
        });

        right_scroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageSlider.smoothScrollToPosition(sliderLayoutManager.findLastVisibleItemPosition() + 1);
            }
        });
//        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//
//switch (item.getItemId())
//{
//    case R.id.action_home:
//        break;
//    case R.id.action_panchangam:
//        break;
//}
//                return false;
//            }
//        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        MenuItem searchViewItem = menu.findItem(R.id.appSearchBar);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        return super.onCreateOptionsMenu(menu);

    }

    private  void init()
    {progressDialog=new Dialog(MainActivity.this);
        bottomNavigationView=findViewById(R.id.bottomNavigationView);
        right_scroll=findViewById(R.id.right_scroll);
        left_scroll=findViewById(R.id.left_scroll);
        nav_home_layout=findViewById(R.id.nav_home_layout);
        nav_temple_layout=findViewById(R.id.nav_temple_layout);
        imageSlider=findViewById(R.id.imageSlider);
        tvTitle=findViewById(R.id.tvTitle);
        templeList=findViewById(R.id.templeList);
        toolbar = findViewById(R.id.toolbar);
        drawerLayout=findViewById(R.id.drawerLayout);
    }

    @Override
    protected void onResume() {
        super.onResume();
setTemples();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.nav_temple_layout:
                Intent templeListIntent=new Intent(MainActivity.this,TempleList.class);
                startActivity(templeListIntent);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_home_layout:
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
        }
    }
    private  void setTemples()
    {
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<TempleListResponse>call=apiInterface.templeList();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<TempleListResponse>() {
                    @Override
                    public void onResponse(Call<TempleListResponse> call, Response<TempleListResponse> response) {

                        if (response.code()==200)
                        {
                            progressDialog.dismiss();
                            dataBean=response.body().getData();
                            Log.d("SIZE",String.valueOf(dataBean.size()));
                            templeList.setAdapter(new TempleListAdapter(MainActivity.this,dataBean, new CustomItemClickListener() {
                                @Override
                                public void onItemClick(View v, String id, String value) {
                                    Intent intent=new Intent(MainActivity.this,TempleDetail.class);
                                    intent.putExtra("id",id);
                                    startActivity(intent);
                                }
                            }));


                        }
                        else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(MainActivity.this, "No Data Found", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<TempleListResponse> call, Throwable t) {
                        progressDialog.dismiss();

                    }
                });
            }
        };
         templeListThread =new Thread(runnable);
        templeListThread.start();
    }
}