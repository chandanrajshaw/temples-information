package kashyap.chandan.templesinformation.ResponseClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TempleListResponse {

    @Expose
    @SerializedName("data")
    private List<DataBean> data;
    @Expose
    @SerializedName("status")
    private StatusBean status;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public static class DataBean {
        @Expose
        @SerializedName("area_name")
        private String area_name;
        @Expose
        @SerializedName("city_name")
        private String city_name;
        @Expose
        @SerializedName("state_name")
        private String state_name;
        @Expose
        @SerializedName("datetime")
        private String datetime;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("seo_description")
        private String seo_description;
        @Expose
        @SerializedName("seo_keyword")
        private String seo_keyword;
        @Expose
        @SerializedName("seo_title")
        private String seo_title;
        @Expose
        @SerializedName("website")
        private String website;
        @Expose
        @SerializedName("email")
        private String email;
        @Expose
        @SerializedName("mobile_no")
        private String mobile_no;
        @Expose
        @SerializedName("address")
        private String address;
        @Expose
        @SerializedName("area_id")
        private String area_id;
        @Expose
        @SerializedName("city_id")
        private String city_id;
        @Expose
        @SerializedName("state_id")
        private String state_id;
        @Expose
        @SerializedName("god_id")
        private String god_id;
        @Expose
        @SerializedName("temple_description")
        private String temple_description;
        @Expose
        @SerializedName("timage")
        private String timage;
        @Expose
        @SerializedName("temple_name")
        private String temple_name;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("user_id")
        private String user_id;
        @Expose
        @SerializedName("id")
        private String id;

        public String getArea_name() {
            return area_name;
        }

        public void setArea_name(String area_name) {
            this.area_name = area_name;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public String getState_name() {
            return state_name;
        }

        public void setState_name(String state_name) {
            this.state_name = state_name;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getSeo_description() {
            return seo_description;
        }

        public void setSeo_description(String seo_description) {
            this.seo_description = seo_description;
        }

        public String getSeo_keyword() {
            return seo_keyword;
        }

        public void setSeo_keyword(String seo_keyword) {
            this.seo_keyword = seo_keyword;
        }

        public String getSeo_title() {
            return seo_title;
        }

        public void setSeo_title(String seo_title) {
            this.seo_title = seo_title;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile_no() {
            return mobile_no;
        }

        public void setMobile_no(String mobile_no) {
            this.mobile_no = mobile_no;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getArea_id() {
            return area_id;
        }

        public void setArea_id(String area_id) {
            this.area_id = area_id;
        }

        public String getCity_id() {
            return city_id;
        }

        public void setCity_id(String city_id) {
            this.city_id = city_id;
        }

        public String getState_id() {
            return state_id;
        }

        public void setState_id(String state_id) {
            this.state_id = state_id;
        }

        public String getGod_id() {
            return god_id;
        }

        public void setGod_id(String god_id) {
            this.god_id = god_id;
        }

        public String getTemple_description() {
            return temple_description;
        }

        public void setTemple_description(String temple_description) {
            this.temple_description = temple_description;
        }

        public String getTimage() {
            return timage;
        }

        public void setTimage(String timage) {
            this.timage = timage;
        }

        public String getTemple_name() {
            return temple_name;
        }

        public void setTemple_name(String temple_name) {
            this.temple_name = temple_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class StatusBean {
        @Expose
        @SerializedName("message")
        private String message;
        @Expose
        @SerializedName("code")
        private int code;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }
    }
}
