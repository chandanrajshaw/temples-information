package kashyap.chandan.templesinformation.Retrofit;


import kashyap.chandan.templesinformation.ResponseClasses.TempleDetailResponse;
import kashyap.chandan.templesinformation.ResponseClasses.TempleListResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIInterface {
    @FormUrlEncoded
    @Headers("x-api-key:temple@123")
    @POST("Admindashservice/view_temple")
    Call<TempleDetailResponse> getTempleDetails(@Field("temple_id") String temple_id);




    @Headers("x-api-key:temple@123")
    @GET("Admindashservice/admindashtemple")
    Call<TempleListResponse> templeList();


}
