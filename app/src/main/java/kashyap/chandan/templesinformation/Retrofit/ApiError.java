package kashyap.chandan.templesinformation.Retrofit;

import java.io.Serializable;

public class ApiError {


    private StatusBean status;

    public StatusBean getStatus() {
        return status;
    }
    public void setStatus(StatusBean status) {
        this.status = status;
    }
    public static class StatusBean implements Serializable {
        /**
         * code : 401
         * message : Sorry! Password does not matched
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
