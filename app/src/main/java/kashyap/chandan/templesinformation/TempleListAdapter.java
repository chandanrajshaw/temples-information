package kashyap.chandan.templesinformation;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.templesinformation.ResponseClasses.TempleListResponse;

public class TempleListAdapter extends RecyclerView.Adapter<TempleListAdapter.MyViewHolder> {
    Context context;
    List<TempleListResponse.DataBean> data=new ArrayList<>();
    CustomItemClickListener customItemClickListener;
    public TempleListAdapter(Context context, List<TempleListResponse.DataBean> data, CustomItemClickListener customItemClickListener) {
        this.context=context;
this.data=data;
this.customItemClickListener=customItemClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.temple_list_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

holder.templeAddress.setText(data.get(position).getArea_name()+","+data.get(position).getCity_name());
        Log.i("SIZE",String.valueOf(data.get(position).getArea_name()+","+data.get(position).getCity_name()));
holder.templeName.setText(data.get(position).getTemple_name());
        Picasso.get().load("http://www.igranddeveloper.xyz/templeinfo/"+data.get(position).getTimage()).placeholder(R.drawable.loading).error(R.drawable.ic_no_preview).into(holder.templeImage);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView templeImage,detail;
        TextView templeName,templeAddress;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            templeImage=itemView.findViewById(R.id.templeImage);
            templeName=itemView.findViewById(R.id.templeName);
            templeAddress=itemView.findViewById(R.id.templeAddress);
            detail=itemView.findViewById(R.id.detail);
            View.OnClickListener clickListener=new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    customItemClickListener.onItemClick(view,data.get(getAdapterPosition()).getId(),"");
                }
            };
            detail.setOnClickListener(clickListener);
        }
    }
}
